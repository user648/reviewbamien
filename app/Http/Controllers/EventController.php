<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use App\Http\Requests\EventRequest;

class EventController extends Controller
{
    // Api lấy danh tất cả các sự kiện
    public function getListEvents()
    {
        $data = Event::all();
        return response()->json($data);
    }

    public function getDetailEvent(Request $request)
    {
        if ($request->event_id) {
            $data = Event::where('id', $request->event_id)->first()->toArray();
            return response()->json($data);
        } else {
            return response()->json('ID sự kiện không được để trống');
        }
    }

    public function AddEvent(EventRequest $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required', 
        //     'avatar' => 'required', 
        //     'describe' => 'required', 
        //     'venue' => 'required', 
        //     'start_time' => 'required', 
        //     'end_time' => 'required', 
        //     'map' => 'required', 
        //     'organization' => 'required', 
        // ], [
        //     'name.required' => "Tên không được để trống",
        //     'avatar.required' => "Ảnh đại diện không được để trống",
        //     'describe.required' => "Mô tả không được để trống",
        //     'venue.required' => "Địa điểm không được để trống",
        //     'start_time.required' => "Thời gian bắt đầu không được để trống",
        //     'end_time.required' => "Thời gian kết thúc không được để trống",
        //     'map.required' => "Iframe google map không được để trống",
        //     'organization.required' => "Tổ chức không được để trống",
        // ]);
        
        // if ($validator->fails()) {
        //     return response()->json($validator->errors()->first());
        // } else {
            $data = Event::create($request->all());
            return response()->json($data);
        // }
    }

    public function DeleteEvent(Request $request)
    {
        if ($request->event_id) {
            $data = Event::where('id', $request->event_id)->delete();
            return response()->json($data);
        } else {
            return response()->json('ID sự kiện không được để trống');
        }
    }
}
