<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required', 
            'avatar' => 'required', 
            'describe' => 'required', 
            'venue' => 'required', 
            'start_time' => 'required', 
            'end_time' => 'required', 
            'map' => 'required', 
            'organization' => 'required', 
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => "Tên không được để trống",
            'avatar.required' => "Ảnh đại diện không được để trống",
            'describe.required' => "Mô tả không được để trống",
            'venue.required' => "Địa điểm không được để trống",
            'start_time.required' => "Thời gian bắt đầu không được để trống",
            'end_time.required' => "Thời gian kết thúc không được để trống",
            'map.required' => "Iframe google map không được để trống",
            'organization.required' => "Tổ chức không được để trống",
        ];
    }
}
