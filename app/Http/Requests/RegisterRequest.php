<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:2,100',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|confirmed|min:3',
            'password_confirmation' => 'required'
        ];
    }
    public function messages() {
        return [
            'name.required' => 'Tên không được để trống!',
            'name.string' => 'Tên không được có ký tự đặc biệt!',
            'name.between' => 'Tên chỉ được phép có độ dài từ 2 đến 100 ký tự',
            'email.required' => 'Email không được để trống!',
            'email.email' => 'Sai định dạng email!',
            'email.unique' => 'Email đã tồn tại!',
            'password.confirmed' => 'Mật khẩu nhập lại chưa khớp!',
            'password.required' => 'Mật khẩu không được để trống!',
            'password.min' => 'Mật khẩu tối thiểu có 3 ký tự!',
            'password_confirmation.required' => 'Nhập lại mật khẩu không để trống!',
        ];
    }
}
