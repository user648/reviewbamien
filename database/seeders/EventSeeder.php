<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('events')->insert([
                [
                    'name' => "Sự kiện '$i'",
                    'avatar' => "https://i.pinimg.com/564x/4b/90/aa/4b90aa86a968373193848f4a94cd6bc1.jpg",
                    'describe' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                    'venue' => "Địa điểm '$i'",
                    'start_time' => "2022-01-01 00:00:00",
                    'end_time' => "2022-01-02 00:00:00",
                    'map' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.9883280769022!2d105.78631351476267!3d20.993104986017727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acb6910fcf09%3A0x4271d83daba3d31!2sThe%20Light%20Trung%20V%C4%83n!5e0!3m2!1svi!2s!4v1655284305179!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                    'organization' => "Ban tổ chức '$i'",
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            ]);
        } 
    }
}