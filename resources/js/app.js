require('./bootstrap');
import Vue from 'vue';
import App from './App.vue';
import router  from './routes/route.js';
import store from './store';
new Vue({
    render: h=>h(App),
    router,
    store
}).$mount('#app')