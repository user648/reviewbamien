//* IMPORT IN ROUTES

// VIEW PAGE HOME
export { default as Home } from "./views/home/pages/Home.vue";
export { default as HomePage } from "./views/home/Index.vue";
export { default as IndexContact } from "./views/contact/Index.vue";
export { default as DetailEvent } from "./views/home/pages/DetailEvent.vue";
export { default as Events } from "./views/home/pages/Events.vue";

// VIEW PAGE ADMIN
export { default as Admin } from "./views/admin/Index.vue";
export { default as Dashboard } from "./views/admin/pages/Dashborad.vue";
export { default as EventsAdmin } from "./views/admin/pages/Events.vue";
export { default as AddEventAdmin } from "./views/admin/pages/AddEvent.vue";
export { default as EditEventAdmin } from "./views/admin/pages/EditEvent.vue";


export { default as Login } from "./views/page/Login.vue";
export { default as Register } from "./views/page/Register.vue";
export { default as ForgotPassword } from "./views/page/ForgotPassword.vue";
export { default as ResetPassword } from "./views/page/ResetPassword.vue";
export { default as ERROR404 } from "./views/page/404.vue";
export { default as ERROR500 } from "./views/page/500.vue";
