// LAYOUT COMPONENTS HOME 
export { default as Header } from './home/Header.vue';
export { default as Footer } from './home/Footer.vue';
export { default as SocialMisc } from './home/Social&Misc.vue';
export { default as Search } from './home/Search.vue';

// LAYOUT COMPONENTS ADMIN 
export { default as Sidebar } from './admin/Sidebar.vue'
export { default as UserSearch } from './admin/UserSearch.vue'

