// ICON
import 'boxicons/css/boxicons.min.css'


//CSS
import '../css/app.css'
import '../css/layouts/header.css'
import '../css/layouts/footer.css'
import '../css/layouts/social&misc.css'
import '../css/layouts/search.css'
import '../css/views/home.css'
import '../css/views/contact.css'
import '../css/views/events.css'
import '../css/views/detail-event.css'
import '../../public/vendor/izimodal/css/iziModal.min.css'
import '../../public/vendor/izimodal/js/iziModal.min'
