import Vue from "vue";
import VueRouter from "vue-router";
import {
    Home,
    HomePage,
    DetailEvent,
    Dashboard,
    Admin,
    IndexContact,
    Login,
    EventsAdmin,
    AddEventAdmin,
    EditEventAdmin,
    Events,
    Register,
    ForgotPassword,
    ResetPassword,
    ERROR404,
    ERROR500,
} from "../components";
Vue.use(VueRouter);
const checkRegisAndLogin = function(to, from, next) {
    if(!localStorage.getItem('token_login')) {
        next()
        return;
    }
    else if(localStorage.getItem('role_user') == '1' && localStorage.getItem('token_login')) {
        next('/admin')
        return
    }
    else {
        next('/')
        return
    }
}
export default new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: HomePage,
            name: "HomePage",
            children: [
                {
                    path: '',
                    component: Home,
                    name: 'Home'
                },
                {
                    path: 'events',
                    component: Events,
                    name: 'Events',
                },
                {
                    path: 'events/detail/:event_id',
                    component: DetailEvent,
                    name: 'DetailEvent'
                },
            ],
        },
        {
            path: "/contact",
            component: IndexContact,
            name: "Contact",
        },
        {
            path: "/admin",
            component: Admin,
            name: "Admin",
            beforeEnter: function(to, from, next) {
                if(localStorage.getItem('role_user') == '1' && localStorage.getItem('token_login')) {
                    next()
                    
                }else {
                    next('/404')
                }
            },
            children: [
                {
                    path: "",
                    component: Dashboard,
                    name: "Dashboard",
                    
                },
                {
                    path: "events",
                    component: EventsAdmin,
                    name: "EventsAdmin",
                },
                {
                    path: "add-event",
                    component: AddEventAdmin,
                    name: "AddEventAdmin",
                },
                {
                    path: "edit-event/:event_id",
                    component: EditEventAdmin,
                    name: "EditEventAdmin",
                },
            ]
        },
        {
            path: "/login",
            component: Login,
            name: "Login",
            beforeEnter: checkRegisAndLogin
        },
        {
            path: "/register",
            component: Register,
            name: "Register",
            beforeEnter: checkRegisAndLogin
        },
        {
            path: "/forgot-password",
            component: ForgotPassword,
            name: "ForgotPassword",
            beforeEnter: checkRegisAndLogin
        },
        {
            path: "/reset-password",
            component: ResetPassword,
            name: "ResetPassword",
            beforeEnter: checkRegisAndLogin
        },
        {
            path: "/404",
            component: ERROR404,
            name: "ERROR404",
        },
        {
            path: "/500",
            component: ERROR500,
            name: "ERROR500",
        },
    ]
});