export const LIST_EVENTS = "LIST_EVENTS";
export const DETAIL_EVENT = "DETAIL_EVENT";
export const ADD_EVENT = "ADD_EVENT";
export const EDIT_EVENT = "EDIT_EVENT";
export const DELETE_EVENT = "DELETE_EVENT";

