import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import user from './modules/user'
import event from './modules/event'
const storeConfigs = {
    modules: {
        user,
        event
    }
}
const store = new Vuex.Store(storeConfigs)

export default store