import {
	LIST_EVENTS,
	DETAIL_EVENT,
	ADD_EVENT,
	EDIT_EVENT,
	DELETE_EVENT
} from "../actions/event";
import router from '../../routes/route'
const state = {
	events: [],
	event_item: '',
	validateAddForm: {
		name: '',
		avatar: '',
		describe: '',
		start_time: '',
		end_time: '',
		map: '',
		venue: '',
		organization: '',
	}
};

const getters = {
	getEvents: state => state.events,
	getEventItem: state => state.event_item,
	getValidateAddForm: state => state.validateAddForm
};

const mutations = {
	[LIST_EVENTS]: (state, item) => {
		state.events = item;
	},
	[DETAIL_EVENT]: (state, item) => {
		state.event_item = item;
	},
	setValidateAddForm(state, data) {
		if(data.name) {
			state.validateAddForm.name = data.name[0]
		}else {
			state.validateAddForm.name = ''
		}

		if(data.avatar) {
			state.validateAddForm.avatar = data.avatar[0]
		}else {
			state.validateAddForm.avatar = ''
		}

		if(data.describe) {
			state.validateAddForm.describe = data.describe[0]
		}else {
			state.validateAddForm.describe = ''
		}

		if(data.start_time) {
			state.validateAddForm.start_time = data.start_time[0]
		}else {
			state.validateAddForm.start_time = ''
		}

		if(data.end_time) {
			state.validateAddForm.end_time = data.end_time[0]
		}else {
			state.validateAddForm.end_time = ''
		}

		if(data.map) {
			state.validateAddForm.map = data.map[0]
		}else {
			state.validateAddForm.map = ''
		}

		if(data.venue) {
			state.validateAddForm.venue = data.venue[0]
		}else {
			state.validateAddForm.venue = ''
		}

		if(data.organization) {
			state.validateAddForm.organization = data.organization[0]
		}else {
			state.validateAddForm.organization = ''
		}


	}
};

const actions = {
	[LIST_EVENTS]: ({ commit }) => {
        axios({
			url: "/api/get-list-events",
			method: "POST",
		})
			.then((res) => {
				commit(LIST_EVENTS, res.data);
			})
			.catch((e) => {
                console.log(e);
			});
	}, 
	[DETAIL_EVENT]: ({ commit }, id) => {
		axios({
			url: "/api/get-detail-event",
			method: "POST",
			data: { event_id: id }
		})
			.then((res) => {
				console.log(res.data);
				commit(DETAIL_EVENT, res.data);
			})
			.catch((e) => {
                console.log(e);
			});
	},
	[ADD_EVENT]: ({ commit, dispatch }, formData) => {
		axios({
			url: "/api/add-event",
			method: "POST",
			data: formData
		})
			.then((res) => {
				router.push({name: 'EventsAdmin'});
				commit('setValidateAddForm', '')
				dispatch(LIST_EVENTS);
			})
			.catch((e) => {
				commit('setValidateAddForm', e.response.data.errors)
                console.log(e.response.data.errors);
			});
	},
	[EDIT_EVENT]: ({ commit, dispatch }, formData) => {
		axios({
			url: "/api/edit-event",
			method: "PUT",
			data: formData
		})
			.then((res) => {
				dispatch(LIST_EVENTS);
			})
			.catch((e) => {
                console.log(e);
			});
	},
	[DELETE_EVENT]: ({ commit, dispatch }, id) => {
		axios({
			url: "/api/delete-event",
			method: "DELETE",
			data: { event_id: id }
		})
			.then((res) => {
				dispatch(LIST_EVENTS);
			})
			.catch((e) => {
                console.log(e.response.data.errors);
			});
	}
};

export default {
	state,
	getters,
	mutations,
	actions,
};