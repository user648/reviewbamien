// import Axios from "axios"
import router from '../../routes/route'
const state = {
    alertValidate: {
        name: '',
        email: '',
        password: '',
        password_confirmation: ''
    },
    alertValidateLogin: {
        email: '',
        password: '',
    },
    validInput: {
        name: false,
        email: false,
        password: false,
        password_confirmation: false
    },
    validInputLogin: {
        email: false,
        password: false,
    },
    statusAuthen: false,
    role: '',
    allowed: false
}
const getters = {
    getAlertValidate: (state) => state.alertValidate,
    getAlertValidateLogin: (state) => state.alertValidateLogin,
    getValidInput: (state) => state.validInput,
    getValidInputLogin: (state) => state.validInputLogin,
    getRole: (state) => state.role,
    getStatusAuthen: (state) => state.statusAuthen,
    getAllowed: (state) => state.allowed,
}
const mutations = {
    setAlertValidate(state, text) {
        if(text.name) {
            state.alertValidate.name = text.name[0]
        } else {
            state.alertValidate.name = ''
        }
        if(text.email) {
            state.alertValidate.email = text.email[0]
        } else {
            state.alertValidate.email = ''
        }
        if(text.password) {
            state.alertValidate.password = text.password[0]
        } else {
            state.alertValidate.password = ''
        }
        if(text.password_confirmation) {
            state.alertValidate.password_confirmation = text.password_confirmation[0]
        } else {
            state.alertValidate.password_confirmation = ''
        }
    },
    setAlertValidateLogin(state, text) {
        if(text.email) {
            state.alertValidateLogin.email = text.email[0]
        } else {
            state.alertValidateLogin.email = ''
        }
        if(text.password) {
            state.alertValidateLogin.password = text.password[0]
        } else {
            state.alertValidateLogin.password = ''
        }
    },
    setRole(state, data) {
        state.role = data
    },
    changeStatus(state, bool) {
        state.statusAuthen = bool
    },
    setAllowed(state, bool) {
        state.allowed = bool
    }
}
const actions = {
    REGISTER_USER({commit, state}, form) {
        axios.post('api/admin/register', form)
            .then((res) => {
                commit('setAlertValidate', '')
                $("#modal-regis").iziModal({
                    title: 'Đăng ký tài khoản thành công!',
                    headerColor: 'rgb(2 178 105)',
                    timeout: 2700,
                    timeoutProgressbar: true,
                    top: 0,
                    icon: "bx bx-check",
                    iconColor: '#fff',
                    overlay: false,
                    autoOpen: 500
                })
                setTimeout(() => {
                    router.push({ name: 'Login' })
                }, 3000)
            })
            .catch(error => {
                console.log("ERROR: ", error.response.data.errors)
                commit('setAlertValidate',  error.response.data.errors)
                if(state.alertValidate.name != '') {
                    state.validInput.name = true
                }else {
                    state.validInput.name = false
                }
                if(state.alertValidate.email != '') {
                    state.validInput.email = true
                }else {
                    state.validInput.email = false
                }
                if(state.alertValidate.password != '') {
                    state.validInput.password = true
                }else {
                    state.validInput.password = false
                }
                if(state.alertValidate.password_confirmation != '') {
                    state.validInput.password_confirmation = true
                }else {
                    state.validInput.password_confirmation = false
                }

            })
    },
    async LOGIN_USER({ commit }, form) {
        await axios.post('api/admin/login', form)
            .then(res => {
                commit('setRole', res.data.role)
                commit('setAllowed', true)
                commit('setAlertValidateLogin', '')
                localStorage.setItem('token_login', res.data.access_token)
                localStorage.setItem('role_user', res.data.role)
                localStorage.setItem('info_user', JSON.stringify(res.data.user))
            })
            .catch(error => {
                if(error.response.data.wrong) {
                    $("#modal-login").iziModal({
                        title: `${error.response.data.wrong}`,
                        headerColor: 'rgb(189, 91, 91)',
                        timeout: 5000,
                        timeoutProgressbar: true,
                        top: 0,
                        icon: "bx bx-error-circle",
                        iconColor: '#fff',
                        overlay: false,
                        autoOpen: 500
                    })
                }
                if(error.response.data.invalid) {
                    $("#modal-login-invalid").iziModal({
                        title: `${error.response.data.invalid}`,
                        headerColor: 'rgb(189, 91, 91)',
                        timeout: 5000,
                        timeoutProgressbar: true,
                        top: 0,
                        icon: "bx bx-error-circle",
                        iconColor: '#fff',
                        overlay: false,
                        autoOpen: 500
                    })
                }

                console.log("ERROR: ",error.response.data);
                commit('setAlertValidateLogin',  error.response.data.errors)
                commit('setAllowed', false)
                if(state.alertValidateLogin.email != '') {
                    state.validInputLogin.email = true
                }else {
                    state.validInputLogin.email = false
                }
                if(state.alertValidateLogin.password != '') {
                    state.validInputLogin.password = true
                }else {
                    state.validInputLogin.password = false
                }
            })
    },
   LOGOUT_USER({commit, state}) {
        axios.get('api/admin/logout')
            .then(res => {
                commit('setRole', '')
                commit('changeStatus', false)
                localStorage.removeItem('role_user')
                localStorage.removeItem('token_login')
                localStorage.removeItem('info_user')
                $("#modal-logout").iziModal({
                    title: 'Đã đăng xuất tài khoản!',
                    headerColor: 'rgb(2 178 105)',
                    timeout: 5000,
                    timeoutProgressbar: true,
                    top: 0,
                    icon: "bx bx-check",
                    iconColor: '#fff',
                    overlay: false,
                    autoOpen: 500
                })
                router.push({ name: 'Login' })
            })
            .catch(error => {
                console.log("ERROR: ", error.response.data.errors)
            })
    },
   
   
}
export default {state, getters, mutations, actions }