<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Review Ba Mien</title>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="120x120"
            href="{{url('')}}/images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32"
            href="{{url('')}}/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16"
            href="{{url('')}}/images/favicon/favicon-16x16.png">
        <link rel="manifest" href="{{url('')}}/images/favicon/site.webmanifest">
        <link rel="mask-icon" href="{{url('')}}/images/favicon/safari-pinned-tab.svg"
            color="#ffffff">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <!-- Notyf -->
        <link type="text/css" href="{{url('')}}/vendor/notyf/notyf.min.css"
            rel="stylesheet">

        <!-- Volt CSS -->
        <link type="text/css" href="{{url('')}}/css/volt.css" rel="stylesheet">

    </head>
    <body>
        <div id="app"></div>

        <script src="{{mix('js/app.js')}}"></script>
        <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
        <!-- Core -->
        <script src="{{url('')}}/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
        <script src="{{url('')}}/vendor/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Vendor JS -->
        <script src="{{url('')}}/vendor/onscreen/dist/on-screen.umd.min.js"></script>

        <!-- Slider -->
        <script src="{{url('')}}/vendor/nouislider/distribute/nouislider.min.js"></script>

        <!-- Smooth scroll -->
        <script src="{{url('')}}/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js">
        </script>

        <!-- Charts -->
        <script src="{{url('')}}/vendor/chartist/dist/chartist.min.js"></script>
        <script
            src="{{url('')}}/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js">
        </script>

        <!-- Datepicker -->
        <script src="{{url('')}}/vendor/vanillajs-datepicker/dist/js/datepicker.min.js">
        </script>

        <!-- Sweet Alerts 2 -->
        <script src="{{url('')}}/vendor/sweetalert2/dist/sweetalert2.all.min.js"></script>

        <!-- Moment JS -->
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js">
        </script>

        <!-- Vanilla JS Datepicker -->
        <script src="{{url('')}}/vendor/vanillajs-datepicker/dist/js/datepicker.min.js">
        </script>

        <!-- Notyf -->
        <script src="{{url('')}}/vendor/notyf/notyf.min.js"></script>

        <!-- Simplebar -->
        <script src="{{url('')}}/vendor/simplebar/dist/simplebar.min.js"></script>

        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>

        <!-- Volt JS -->
        <script src="{{url('')}}/js/volt.js"></script>
        <script>
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            const forms = document.querySelectorAll('.needs-validation');

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms).forEach((form) => {
                form.addEventListener('submit', (event) => {
                    if (!form.checkValidity()) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        </script>
    </body>
</html>
