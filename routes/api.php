<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'admin'], function() {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::post('/user-profile', [AuthController::class, 'userProfile']);
    Route::post('/change-pass', [AuthController::class, 'changePassWord']);

});

Route::group(['middleware' => 'api', 'prefix' => '/'], function() {
    // Route::post('/user-profile', [AuthController::class, 'userProfile']);
    Route::post('/get-list-events', [EventController::class, 'getListEvents']);
    Route::post('/get-detail-event', [EventController::class, 'getDetailEvent']);
    Route::post('/add-event', [EventController::class, 'AddEvent']);
    Route::put('/edit-event', [EventController::class, 'EditEvent']);
    Route::delete('/delete-event', [EventController::class, 'DeleteEvent']);
});


